# Monter un site similaire avec GitLab

1. Cloner le dépôt https://github.com/sergiokopplin/indigo
2. Créer un nouveau projet sur une instance GitLab quelconque.
3. Importer sur ce projet le dépôt cloné.
4. `Settings` > `CI/CD` > `Runners` > `Enable shared Runners`
5. Copier le fichier [.gitlab-ci.yml](/.gitlab-ci.yml) de ce dépôt sur votre nouveau projet.
6. Adapter `_config.yml` à vos besoins. Vous pouvez regarder ce qui a été fait [ici](/_config.yml) pour vous inspirer.
7. Lancer la CI
8. Le site sera accessible sur une URL du type `https://<login>.<instance_gitlab>/<projet>` (exemple: `https://elus_etu.gitlab.utc.fr/website`)
