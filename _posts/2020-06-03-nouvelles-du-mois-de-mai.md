---
title: Dernières nouvelles de vos élu⋅es
layout: post
date: 2020-06-03 22:00                         # Laissez 2099 si c'est un brouillon. Pour l'heure,
                                                 # mettez au moins 2h avant l'heure actuelle (bug GitLab)
image: /assets/images/profile_pictures/all.png   # Apparaît lorsque vous partagez le lien
headerImage: true                               # Mettez 'true' pour mettre l'image précédente en entête du post
category: blog
author: eluscevu
---
#### ... Alors que nous commençons à voir le bout du semestre, voici un message des vos élu⋅es concernant les activités menées durant les mois d'avril et de mai dans les différents conseils, et les projets pour les semaines à venir ! Bonne lecture :wink: ...

### SOMMAIRE

- [Conseil des Études et de la Vie Universitaire](https://elus_etu.gitlab.utc.fr/nouvelles-du-mois-de-mai/#conseil-des-études-et-de-la-vie-universitaire)
- [Conseil d'administration](https://elus_etu.gitlab.utc.fr/nouvelles-du-mois-de-mai/#conseil-dadministration)
- [Génie Biologique](https://elus_etu.gitlab.utc.fr/nouvelles-du-mois-de-mai/#génie-biologique)
- [Ingénierie Mécanique](https://elus_etu.gitlab.utc.fr/nouvelles-du-mois-de-mai/#ingénierie-mécanique)
- [Génie des Procédés](https://elus_etu.gitlab.utc.fr/nouvelles-du-mois-de-mai/#génie-des-procédés)
- [Génie Urbain](https://elus_etu.gitlab.utc.fr/nouvelles-du-mois-de-mai/#génie-urbain)
- [Génie Informatique](https://elus_etu.gitlab.utc.fr/nouvelles-du-mois-de-mai/#génie-informatique)
- [Technologie et Sciences de l'Homme](https://elus_etu.gitlab.utc.fr/nouvelles-du-mois-de-mai/#technologie-et-sciences-de-lhomme)
- [Master](https://elus_etu.gitlab.utc.fr/nouvelles-du-mois-de-mai/#master)
- [Info service d'écoute psychologique](https://elus_etu.gitlab.utc.fr/nouvelles-du-mois-de-mai/#info-service-d%C3%A9coute-psychologique)

# Conseil des Études et de la Vie Universitaire

## CÉVU du 4 juin 2020

Ce jeudi 4 juin a eu lieu un conseil  dont vous pouvez retrouver [l'ODJ sur l'ENT](https://webapplis.utc.fr/ent/services/services.jsf?sid=648) (et plus tard au même endroit le CR). Nous avons notamment abordé **l'organisation de la rentrée prochaine**, l'ouverture d'une **nouvelle UV TSH** : _WE01_, les avancées du **Collectif Ingénierie Durable**, la **réforme du GI**, l'évolution du **projet pédagogique du GP**, la **CVEC** (contribution destinée à favoriser l’accompagnement social, sanitaire, culturel et sportif des élèves et étudiants et à conforter les actions de prévention et d’éducation à la santé réalisées au profit des étudiants).

Pour l'organisation de la rentrée prochaine, une base de travail a été présentée par le DFP donnant les grandes orientations du semestre d'automne. Pour cela ont été élaborés plusieurs scénarios à partir de différents principes et hypothèses.
Une des nouvelles informations dont nous avons pris connaissance est, en plus du retour à la notation ABCDEF, l'attribution d'un crédit automatique en plus par UV validée à la fin du semestre. L'inscription sera limitée à 30 crédits par semestre, pour ne pas surcharger les UVs. Par exemple,
Si on prend 5 UVS à 28 crédits au total : 2 CS 2 TM 1 TSH, et qu'on les valide toutes, on aura 28 crédits + 5 automatiques => 33 crédits à la fin du semestre.

Pour d'autres questions sur ce CÉVU, n'hésitez pas à poster un message sur l'équipe Mattermost des élu⋅es ! Nous avons détaillé dans la section suivante les avancées du CID, qui ont été saluées par les membres du CÉVU.

## Collectif Ingénierie Durable

Malgré la fermeture de l'UTC le collectif continue à se réunir toutes les deux semaines pour avancer sur leurs travaux. Pour avoir plus d'informations sur l'organisation, la composition et les travaux du collectif nous vous renvoyons vers [la première présentation que nous vous avons partagé](https://cloud.elus-etu.utc.fr/index.php/s/KMGjNAQzFNx9DND)

--> **Dans un premier temps**, durant le mois de mai, le CID a continué à avancer sur les thématiques des lowtechs avec une présentation des recherches sur les **lowtechs en génie biologique**.
Elle s'est suivie par des échanges avec la direction du département puis une présentation au bureau de département GB. Un "séminaire" est envisagé pour approfondir la question et rassembler des enseignants chercheurs GB intéressés par le sujet pour réfléchir lors d'une journée sur cette question. Quelques exemples de thèmes abordés lors de la présentation des lowtechs en GB : la bioremédiation, l'agroécologie dont la permaculture, la création de matériel dans les fablabs, mais également le principe de "prévenir plutôt que guérir" en santé, car le soin le plus respectueux de l'environnement est celui qu'on n'a pas à réaliser.
Le mot d'ordre : **lowtech n'est pas synonyme d'abandon de l'ingénierie**, certaines des solutions proposées demandent un savoir technique et biologique avancé, et notamment une bonne prise en compte de l'imprévisibilité du support de travail : le vivant.

--> **L'IM n'est pas en reste, avec des travaux engagés sur les éco-matériaux**, et bientôt une présentation sur le thème des lowtechs également.
Quelques pistes de réflexion : pour sélectionner un matériau responsable, il faut penser à tout son cycle de vie, de l'extraction à la fin de vie, essayer de prendre celui qui ne provient pas d'écosystème menacé et qui ne menacera pas ensuite d'autres écosystèmes, envisager son réemploi ou sa recyclabilité, penser à ses modes de transport, à l'impact de son extraction ou de sa transformation... 

--> **Un projet très important du collectif est également en train de prendre forme : la sensibilisation de rentrée**.Le but ? Informer tous les nouveaux entrants sur les enjeux climatiques, sociaux, environnementaux ... mais également proposer le dispositif aux étudiants actuels ainsi qu'aux personnels de l'UTC.
Un énorme travail a déjà été fait, avec pour but de proposer plusieurs rendez-vous aux étudiants lors du semestre prochain, pour ne pas en faire seulement un évènement ponctuel. The Big Conf, fresques du climat... Les activités envisagées sont choisies et préparées _aux petits oignons_ !

--> **Le CID mène également une réflexion sur les termes ou qualificatif que nous souhaiterions donner à la formation d'ingénieur**, car l'expression Ingénierie Durable ne fait pas l'unanimité.
Ce fut l'occasion de débats très riches, qui se poursuivent. (_Si vous avez un avis sur ce sujet, envoyez nous un message !_)

--> **Le dernier bureau TSH a été entièrement dédié à l'échange avec les représentants extérieurs sur le travail du collectif**, voir la partie TSH pour plus d'informations.

--> Le collectif a produit **une note de synthèse résumant 4 enjeux de formation** sur lesquels nous travaillons et comptons travailler, que nous vous invitons à retrouver [sur son cloud](https://cloud.endor.utc.fr/index.php/s/YZdsLrSkHdqLYJ3).

Rappelons également que vous pouvez rejoindre notre Mattermost pour suivre nos discussions voire rejoindre le collectif si vous êtes motivé⋅es : https://team.picasoft.net/id-en-ut/channels/cid-utc---centre-ville

_Contact : eluscevu@utc.fr_

# Conseil d'Administration

Le début de la crise du COVID19 a transféré temporairement l'attention du CA de ses sujets classiques (stratégie d'alliance, stratégie budgétaire, etc.) vers des questions pratiques qui étaient urgentes et notamment l'élaboration du PCA puis du PRA. Pour l'élaboration de ces documents, il y a eu plusieurs réunions entre la direction et les élus CA (étudiants et enseignants).

Suite à cela,  le CA reprend petit à petit ses thématiques classiques avec un souhait fort: **établir une vision stratégique à moyen et long terme**. Pour construire une telle vision, les élus CA (enseignants et étudiants) souhaitent travailler sur trois axes :
- la croissance des effectifs de l'UTC
- la stratégie d'alliances (nationales et internationales)
- le positionnement sur des axes phares (d'un point de vue technologique) notamment l'ingénierie durable.

Le but serait d'établir une feuille de route à moyen et long terme sur chacunes de ces questions afin de briser un court-termisme s'étant insinuer dans la stratégie de l'UTC ces 10 dernières années.
Ce travail a déjà commencé avec **l'audition du collectif ingénierie durable**. Cette audition avait pour d'entrer dans le détail du travail de ce collectif afin de ne pas se limiter à la courte présentation qui sera faite au CA de juin. Il y a une nette envie étudiante comme enseignante de soutenir et d'accompagner la collectif afin que leurs solutions deviennent partie intégrante de l'identité de l'UTC à terme.

**Le prochain CA aura lieu le 18 juin**. En plus des questions budgétaires habituelles, les thématiques suivantes seront notamment abordées:
- Ingénierie durable, Master Erasmus Mundus, Préparation du semestre A20, stratégie d'innovation et points d'information sur les partenariats et les classements de l'UTC.

_Contact : elusca@utc.fr_

# Génie biologique

--> **Le dernier conseil de département a eu lieu le 26 mai.**
Différents points ont été abordé, tout d'abord concernant la réactivité des différents laboratoires, du Fablab et des affaires réglementaires de l'UTC pour **répondre aux besoins des hôpitaux et aux acteurs du secteur de la santé lors de la crise du COVID19**.

### Covid19

Les laboratoires de l'UTC, des chercheurs et des étudiants ont cherché à utiliser des appareils de l'UTC (notamment l'imprimante 3D), mais une certaine réglementation a bloqué le processus engendrant une réponse et un engagement tardif de la part de l'UTC.
Les chercheurs/enseignants ont donc souhaité créer un projet transversal inter branches et départements : Mobilisation UTC post-COVID, de façon à pouvoir réagir rapidement en cas de nouvelles crises.
Différents enseignants ont exprimé leurs points de vue sur le projet, certains ayant été marqués par le temps de latence de la part de l'UTC comparé à d'autres écoles, comme l'UTBM qui a su réagir très rapidement.
Quelques idées ont été proposées : offres de stage en lien avec la crise actuelle, nouvelles PR proposées aux étudiants, collaboration avec les affaires réglementaires de l'UTC et avec les hôpitaux pour comprendre les besoins et leurs contraintes, favoriser les projets inter-branches et valoriser les idées des étudiants... Une demande de financement a été faite à la Direction à la Recherche.

Tout cela doit se faire avec une prise en compte de l'impact environnemental de cette crise : beaucoup d'outils étaient à usage unique pendant la crise, certaines questions se posent sur la fabrication de produits réutilisables (une ingénieure biomédicale en milieu hospitalier, représentante Entreprise, a fait part de cet impact négatif peu pris en compte lors de la crise).
Un des points soulevés était que lors d'une crise, l'impact environnemental est souvent relégué au dernier plan, mais ensuite les habitudes s'installent et il est difficile de modifier le fonctionnement pour limiter ces impacts. La notion de low-tech a été abordée, certains enseignants défendant l'utilisation de technologies simples pour répondre rapidement aux besoins (exemple des masques décathlon repris pour différents usages).

### Présentation du CID

Nous avons également eu **une présentation du Collectif Ingénierie Durable**, présentant les points qui seront apportés aux enseignements GB à la rentrée prochaine (sensibilisation plus importante des enseignants et des étudiants, accompagnements des enseignants pour développer de nouvelles formations, l'adoption d'une démarche low-tech, la redéfinition de la notion d'innovation...). Cela a été peu discuté (manque de temps), mais les élus et participants au Conseil ont approuvé la démarche et acceptent de participer au projet.

### Reprise des cours en septembre

Ensuite, nous avons discuté des questions des étudiants, notamment la reprise des cours en septembre. Différentes solutions ont été apportées : l'option idéale serait de reprendre les cours de façon normale, possible seulement si la maladie est contrôlée (traitement, vaccin etc), ce qui est aujourd'hui peu probable. La deuxième solution, plus envisageable, serait de procéder avec des cours à distance, et des temps de régulation en présentiel (quelques TDs et TPs seraient également maintenus, l'organisation de ceux-ci restent à voir). La troisième option, à éviter, serait imposée par l'État : tous les cours seraient en distanciel.
Pour l'instant les enseignants ont peu de visibilité.

### Problème des UVs en GB

**Nous avons évoqué le problème des UVs en GB**. Certaines manquent de place, et ne sont proposées qu'à un semestre sur deux. Cela engendre un manque d'UVs PCB en GB, et des étudiants n'ont pas la possibilité de faire certaines UVs car elles n'offrent pas suffisament de place. Le sujet sera plus amplement abordé lors du conseil de perfectionnement fin juin/début juillet (on espère!!!!). Des UVs PSF doivent être passées en PCB (point déjà abordé lors du conseil de département en janvier).

### Problématique des stages

Enfin, la problématique des stages a été abordée. Les enseignants et responsables sont conscients de la difficulté à trouver un stage, il n'y aura pas de réorientation si l'étudiant ne trouve pas de stage, la date butoir est à titre d'information mais n'est pas une réelle date butoir. Ils souhaitent informer les étudiants qu'ils ne doivent pas être inquiets, ce n'est pas grave d'un point de vue administratif et "scolaire" s'ils trouvent leur stage cet été, ils seront aidés et suivis par l'administration du département. Pour les TN10, ce n'est pas un problème si les stages commencent après la date "normale" annoncée en début de semestre (pour les TN09, cela pose plus de problème étant donné qu'il faut reprendre les cours). Plus de la moitié des étudiants en TN09 ont trouvé un stage, c'est plutôt encourageant pour les autres.

_Contact : elusgb@utc.fr_

# Ingénierie mécanique

Lors des derniers conseils les principales problématiques abordées étaient les suivantes : 

## Stages

Beaucoup d'étudiants IM ont vu leur stage raccourci/annulé ce semestre à cause de la pandémie ; et seule une minorité d'étudiants devant partir en stage au semestre prochain ont trouvé un stage (quand on compare les chiffres de cette année et de l'année dernière on constate un gros décalage aux mêmes dates). Ajoutons à cela le fait qu'un certain nombre de grands groupes de l'automobile et de l'aéronautique ont annoncé n'accueillir de nouveau des stagiaires qu'à partir de 2021, cela s'annonce compliqué pour la branche IM.
Pour les premiers la question est de savoir si on leur demande de refaire un stage, sachant que l'on distingue les cas des TN09 de ceux des TN10 (le TN10 étant considéré comme plus important pour le diplôme). Dans certains cas les entreprises ont permis aux étudiants de reprendre leur stage après la période de confinement et de prolonger la durée jusqu'à atteindre le bon nombre de semaines. D'autres doivent chercher un nouveau stage. 
Il a été rappelé que la CTI impose un total de 28 semaines de stage au total au long des années d'études, et que dans les conditions actuelles elle a assoupli cette obligation et encouragé les écoles à trouver des solutions adaptées à leur situation. L'UTC réfléchit actuellement à plusieurs solutions en fonction des situations existantes.
Pour ceux n'ayant pas trouvé leur stage TN09, l'idée serait de leur permettre de s'inscrire aux UVs au semestre prochain mais de les encourager à chercher en parallèle un stage encore jusqu'en octobre-novembre, et s'ils trouvent de partir en stage à ce moment là. L'inscirption aux UVs est là pour garantir que le semestre ne soit pas perdu si jamais ils ne trouvaient pas de stage.
Et pour ceux ayant pu réaliser un certain nombre de leurs semaines de stage TN09, il est envisagé de leur demander d'effectuer un travail complémentaire (sous forme bibliographique, ou autre) portant sur un sujet en lien avec le sujet de stage pour compléter et tout de même obtenir des crédits. Toutefois le problème posé par cette solution est celui de la disponibilité des encadrants pour suivre ces travaux. En l'état actuel des choses l'équipe enseignante n'a pas tout à fait les moyens de réaliser cela en plus du reste.

## Rentrée A20

Le problème majeur auquel va être confronté la branche IM est celui du sureffectif. En effet entre les étudiants qui ont vu leur départ à l'étranger annulé et les étudiants qui n'auront pas trouvé de stage, la branche va connaître des problèmes d'effectifs dans les UVs à la rentrée prochaine. Parmi les solutions envisagées, les membres du conseil souhaitent ouvrir une partie des UVs de printemps à l'automne. 
A propos des mesures de distanciation sociale, ils envisagent des modes d'enseignement distanciels avec plus de supports pédagogiques pour rendre le travail le plus autonome possible. Il est aussi envisagé de devoir subdiviser les groupes d'étudiants et de les faire venir en présentiel en alternance pour assurer un minimum le suivi et permettre à tout le monde d'échanger en direct avec les enseignants. 

_Contact : elusim@utc.fr_

# Génie des Procédés

Au niveau du GP, un point sur l'évolution pédagogique du GP a été vu au CÉVU du 4 juin. Ont été présentés les deux point de vigilances suivants pour la branche :
- répondre aux enjeux sociétaux
- renforcer l'attractivité, mieux faire connaitre le GP

Pour cela la démarche suivie fut de mettre un point un état des lieux au niveau du département, puis de définir des macro-orientations, ce qui fit aboutir à 3 filières. Voilà pour un débrief très concis !

En attendant, n'hésitez pas faire remonter vos attentes à vos élu⋅es par mail ou sur Mattermost ; l'équipe pédagogique étant très ouverte à la participation des étudiant⋅es dans les réflexions sur l'évolution du département.

_Contact : elusgp@utc.fr_

# Génie Urbain

Durant le confinement, vos élu⋅es GU n'ont pas chaumé et ont travaillé sur l'élaboration d'un tout nouveau site internet pour promouvoir la branche ... **Retrouvez - en exclusivité ! - les premiers articles ici : https://elus_etu.gitlab.utc.fr/gu/actualites/ !**

Avec le nouveau plan pédagogique, vous avez du voir passé le nom du futur label "Villes et Changement Climatique". Si certain⋅es d'entre vous sont intéréssé⋅es, les élu⋅es vous proposent d'en disctuer sur un canal mattermost dédié au sein de l'équipe 'ID en UT' en y associant des profs (afin de fluidifier la communication, et faire avancer le sujet). Envoyez nous un mail si vous trouvez que ce serait une bonne idée et nous lancerons ce canal de discussions !

_Contact : elusgu@utc.fr_

# Génie Informatique

Plusieurs choses ont été voté récemment au sein du conseil de département GI. Tout d'abord la création de 3 nouvelles uvs : AP01, SR08 et SR09. Voilà en résumé ce que vont proposer ces trois uvs :

- AP01, intitulée "Atelier projet génie informatique", est un atelier projet visant à mettre en œuvre, sur un cas d’étude conséquent proposé, les principes appris par les élèves ingénieur du génie informatique. Les étudiants participants à un atelier auront une demi-journée par semaine pour travailler sur le cas d’étude, proposé par un client (industriel, organisme public, laboratoire, etc.) qui sera impliqué dans le suivi des travaux, et sera en charge de fournir les moyens nécessaires (par ex. : missions, données, petits matériels, etc.) à la bonne poursuite du projet. Son responsable est Sébastien Destercke.

- SR08, intitulée "Cloud et réseaux avancés", est une uv dans laquelle sera faite la présentation des architectures avancées intégrant la chaîne complète de la collecte de données provenant de diverses sources comme l'IoT, leur transport, ainsi que leur stockage et partage pour traitement sur cloud. Elle permettra l'étude des différentes approches cloud (public, privé, hybride) en mettant en avant les avantages et inconvénients des unes par rapport aux autres selon les environnements de déploiement. Ensuite nous étudions les principales technologies cloud et la mise en place de solutions hétérogènes (multi-cloud) et résilientes. Elle abordera aussi les réseaux avancés, en particulier IPv6/5G/SDN, permettant le transport des données vers le cloud. Une étude des protocoles de communication (courte distance ou longue distance) spécifique à l'IoT sera présentée. La dernière partie de l'UV est consacrée à la sécurité du cloud et de l'IoT. Elle comprend l'étude des menaces sur les architectures cloud et IoT ainsi que les solutions (services de sécurité) à intégrer pour mettre en place des architectures cloud et IoT sûres et sécurisées. Son responsable est Abdelmadjid Bouabdallah.

- SR09, intitulée "Projets sûreté & sécurité avec les entreprises", cette uv a pour objectif de faire participer les étudiants à la réalisation d'un projet sous la supervision d'un enseignant et en collaboration avec des entreprises ou des laboratoires de recherche dans les thématiques de la sûreté de fonctionnement et/ou de la sécurité des systèmes informatiques. Son responsable est Mohamed Sallak.

_Le CÉVU du 4 juin a validé à l'unanimité la proposition d'évolution des fillières du GI ainsi que ces 3 nouvelles UVs._

En autre nouvelle intéressante, le conseil a aussi voté un nouveau référenciel de compétences pour le GI. À notre plus grand désarroi, celui-ci ne comporte aucune compétence ayant attrait aux enjeux éthiques, sociaux et environnementaux de notre temps et que nous souhaitons intégrer au parcours du futur ingénieur informatique de l'UTC.

Le conseil s'est dernièrement prononcé favorablement à la demande d'éméritat du professeur très prochainement retraité Pierre Morizet-Mahoudeaux.

Enfin la dernière info en date est la création d'un poste de responsable de branche adjoint, pour décharger quelque peu le travail du responsable de branche du fait de la taille du GI actuellement.
Voilà pour nous c'est tout pour le GI informatique, pour le moment !

_Contact : elusgi@utc.fr_

# Technologie et Sciences de l'Homme

Lors du bureau TSH du mois précédent, 3 nouvelles TSH ont été proposées : 
- modification d'AS01, reprise par Hugues Choplin, qui souhaite mettre l'accent sur les collectifs de l'ingénieur. En effet, l'ingénieur travaille généralement au sein de collectifs, et c'est notamment en les étudiant que l'on peut comprendre les situations auxquelles il fait face et les enjeux de son travail. 
- modification d'EI03, reprise par Hadrien Coutant. L'accent n'est plus mis sur l'interculturalité en entreprise en terme de nationalités différentes, mais en terme de culture personnelle différente. Par exemple, la question du genre en entreprise sera traitée. 
- création de WE01 par Stéphane Crozat, qui portera sur quelques bases d'usage Web et de la théorie du support (introduction au HTML, CSS, Javascript...) ainsi que sur l'utilisation d'outils libres (Scénari, Mattermost, Nextcloud...).

Ces 3 projets ont été validés par le département TSH, ils passeront donc au CEVU.
Par ailleurs, suite à la fin du mandat des responsables du domaine des langues, Mme.Lyne Forest a présenté sa candidature afin d'assurer ce poste pour les deux prochaines années.

**Le bureau TSH de ce mois-ci s'est tenu avec les représentants extérieurs, venant des organismes suivants : Renault, Saint Gobain, Pipplet, L'Hermitage, le CEEBIOS, Negawatt.**
Il a été convenu entre les membres du bureau d'interroger les représentants extérieurs sur les enjeux du Collectif Ingénierie Durable (voir partie sur le CID ci-dessus), présentés dans la note de synthèse abordée dans la partie sur le CID, à travers des questions sur chacun des 4 enjeux. La réunion a été quasi-exclusivement réservée à celà, ce qui a permis des discussions très riches et de prendre le temps de tous intervenir. Nous les avons interrogé sur l'impact de ces enjeux sur le role de l'ingénieur dans leur organisme et avons obtenu des réponses parfois variées, notamment par le fait que les organismes sont vraiments des structures différentes, ce qui permettra au CID de construire son travail en cohérence avec les organismes recrutant des ingénieurs (rappelons que co-construction est très importante au CID). Le bureau est actuellement en train de s'organiser pour faire une analyse complète des échanges ayant eu lieu, que nous vous ferons parvenir dès qu'elle sera bouclée. Globalement, il y a un consensus pour dire qu'il est important que les ingénieurs soient formés pour répondre à ces enjeux, mais selon les organismes la profondeur de cette formation souhaitée varie. 
Nous remercions les intervants extérieurs, tout le bureau, et particulièrement Frédéric Huet qui a préparé le document pour les intervenants avec nous. 

_Contact : elustsh@utc.fr_

# Master

Rien à signaler de particulier pour le moment... Pour toute question contactez : eluscpm@utc.fr avec eluscevu@utc.fr en copie !
:smile:


# Info service d'écoute psychologique 

Enfin, voici une petite info que nous venons de voir passer sur le site du CROUS :

> Happsy Line, un nouveau service d’écoute psychologique a été mis en place pour les étudiants hors Amiens, les videoconsultations sont sur rendez-vous et sont proposées à des horaires flexibles. Les consultations par webcam avec un(e) psychologue sont gratuites.[...]
> L’objectif de l’Happsy Line est d’aider les étudiants à faire face aux situations de mal-être qu’ils peuvent traverser et qui peuvent les empêcher d’avancer sur le plan personnel et scolaire.
Les étudiant(e)s peuvent consulter pour tout type de problème (mal-être, difficultés d’adaptation à l’étranger, phobies, crises d’angoisse, etc.)
> Les entretiens se font via une plateforme de consultation, sont proposés à des horaires flexibles et durent 45 minutes. Les consultations sont offertes et réservées aux étudiants hors Amiens.

Plus de détails et contact sur le site du CROUS : https://www.crous-amiens.fr/actualite/lhappsy-line/
