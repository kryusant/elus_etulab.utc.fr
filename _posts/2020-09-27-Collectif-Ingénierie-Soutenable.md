---
title: "Présentation du Collectif Ingénierie Soutenable"
layout: post
date: 2020-09-27 12:00                           # Laissez 2099 si c'est un brouillon. Pour l'heure,
                                                 # mettez au moins 2h avant l'heure actuelle (bug GitLab)
image: /assets/images/posts/logo-ID-en-UT.png   # Apparaît lorsque vous partagez le lien
headerImage: true                               # Mettez 'true' pour mettre l'image précédente en entête du post
category: projects
author: eluscevu
---
#### SOMMAIRE
- [Les origines du Collectif Ingénierie Soutenable](https://elus_etu.gitlab.utc.fr/Collectif-Ing%C3%A9nierie-Soutenable/#les-origines-du-collectif-ingénierie-soutenable)
- [Les objectifs du collectif](https://elus_etu.gitlab.utc.fr/Collectif-Ing%C3%A9nierie-Soutenable/#les-objectifs-du-collectif)
- [Et concrètement, ça se passe où ?](https://elus_etu.gitlab.utc.fr/Collectif-Ing%C3%A9nierie-Soutenable/#et-concrètement-ça-se-passe-où-)
- [Les travaux du collectif](https://elus_etu.gitlab.utc.fr/Collectif-Ing%C3%A9nierie-Soutenable/#les-travaux-du-collectif)



# Les origines du Collectif Ingénierie Soutenable

Le Collectif Ingénierie Soutenable de l'UTC est l'aboutissement de différents travaux et initiatives de transformation de l'UTC depuis plusieurs années, autant sur le plan de la formation que sur celui de la vie étudiante. En particulier, les travaux réalisés lors de l'activité pédagogique d'intersemestre "ID en UT" de l'été 2019 et la présentation de ces travaux au séminaire inter-UT 2019 ont entraîné une forte dynamique au niveau des étudiant⋅es, notamment lors des élections universitaires d'automne 2019.

La nouvelle équipe d'élu⋅es éudiant⋅es, soutenue par plusieurs enseignant⋅es, a en effet porté le projet de création d'un groupe de travail ou d'un collectif spécialement dédié aux questions de soutenabilité et d'évolution de la formation au sein du Conseil des Études et de la Vie Universitaire.
D'abord nommé *Collectif Ingénierie Durable*, un premier groupe de travail a été officialisé lors du CÉVU du 31 janvier 2020, avant de devenir le *Collectif Ingénierie Soutenable* au semestre de printemps 2020.

L'image ci-dessous retrace les différents mouvements externes et internes à l'UTC liés à la création du CIS entre 2015 et janvier 2020.

![Historique transformation UTC](../assets/images/posts/Historique-ID-en-UT-frise-1-1.png)


Le collectif est composé de membres étudiant·es et enseignant·es élu·es au CÉVU, mais
également de collaborateur·rices étudiant·es et enseignant·es ainsi que des membres du personnel BIATSS ne faisant pas partie de cette instance.

Il s'organise et communique en interne essentiellement sur l'outil Mattermost proposé par [l'association Picasoft](https://picasoft.net/)[^note] , au sein de l'équipe *ID en UT* créée à la suite de la consultation et de la première Activité Pédagogique d'Intersemestre (API) *ID en UT* de l'été 2019.

# Les objectifs du collectif

Le CIS a pour but de réfléchir aux solutions et d'accompagner leur mise en œuvre pour faire évoluer la formation d’ingénieur UTC dans le but de relever les défis sociétaux et environnementaux actuels et futurs, en collaboration avec toutes les parties prenantes. En effet, l'UTC (et plus largement les UT) est un écosystème où interagissent de nombreux acteurs : il est indispensable de tous les impliquer dans le projet afin d'identifier les leviers d’actions et de comprendre le système dans sa complexité.

Le collectif dépasse ainsi un groupe de travail comme il en existe habituellement à l'UTC, en comportant davantage de membres et en travaillant à court, moyen et long terme. 

Le CIS s'est initialement focalisé sur deux chantiers assez radicaux :

• **Comment adapter l'UTC et intégrer les enjeux de soutenabilité à la formation ?** *Il nous faut expérimenter de nouveaux contenus, de nouveaux outils pédagogiques, de nouvelles UV, etc.*

Un premier sous-collectif nommé *Action/Transformation/Expérimentation* a été créé afin de mettre en place des tests, des essais de modèles pédagogiques et de contenus, dans le but de de lancer les choses rapidement, d'avoir des retours, d'étudier ce qui fonctionne ou non, d'apporter du concret au volet prospection, et répondre à la question *"Comment?"*.
Ce volet du travail nous permet d'embarquer les différentes parties prenantes (enseignant·es, étudiant·es, personnel administratif, mais aussi alumnis), de donner du dynamisme à notre travail et dans le même temps de réaliser, en continu, un "état de l'art" interne.
Un des résultats attendu est par exemple la modification du règlement des études pour la rentrée 2021.

• **Quels contenus doivent être enseignés ? Quels sont les compétences de l'ingénieur dans 5 ans ?** *Il nous faut prospecter.*

Un deuxième sous-collectif de *propsection* (plus restreint) est en charge de rechercher et d'identifier les "savoirs" de référence et d'importance (notamment en CS et TM mais également en TSH). Ce travail passe exemple par une veille internationale qui permet d'identifier les nouvelles compétences, des modalités pédagogiques spécifiques, etc. Finalement ce volet répondrait à la question *"Quoi ?"*.
On pourrait résumer le travail de ce second sous-collectif à un état de l’art externe : le but étant d'identifier ce qui se fait déjà ainsi que d'identifier la demande (des jeunes qui se mobilisent, des organismes qui recrutent, des enseignant·es qui veulent faire évoluer leurs formats et contenus pédagogiques...).

Ci-dessous est représenté le schéma de fonctionnement du collectif tel que proposé au CÉVU du 31 janvier 2020.
![Schéma CID 31 janvier 2020](../assets/images/posts/schema_CID.png)


# Et concrètement, ça se passe où ?

L'essentiel des documents et des échanges se trouve sur **l'équipe de discussion Mattermost "ID en UT"**, dans les canaux de discussions commençant par *CIS-*.

Chaque membre de l'UTC peut rejoindre l'équipe de discussion afin de : se tenir informé des actualités du CIS, participer aux réflexions et aux débats, s'investir dans un projet du CIS, etc. 

Pour rejoindre cette équipe, [suivez ce lien](https://team.picasoft.net/id-en-ut/channels/cid-utc---centre-ville) si vous avez déjà un compte sur team.picasoft.net ; ou [suivez ce lien](https://team.picasoft.net/signup_user_complete/?id=3ysc5gizefbpukfu967z5z17oc) pour créer un compte sur team.picasotft.net.

Le canal central à rejoindre s'intitule "CIS UTC - Centre ville", et on retrouve en en-tête de ce canal différents lien vers :
- **[Le document d'initiation à Mattermost](https://cloud.endor.utc.fr/index.php/s/g72iASEP6Jeew3f?path=%2FCID_centre_ville#pdfviewer)**
- [Les principales informations du CIS (membres, réunions, travaux) du semestre P20](https://pad.rhizome-fai.net/s/ryb6Vj0hL#)
- [Le cloud du CIS](https://cloud.endor.utc.fr/index.php/s/g72iASEP6Jeew3f)
- [Le pad des ordres du jour collaboratifs et des comptes-rendus du CIS](https://pad.picasoft.net/p/odj_IDenUT_P20)

Si vous souhaitez rejoindre le collectif ou participer à certains travaux, il suffit de poster un message pour vous présenter sur le canal "CIS UTC - Centre ville".

Une équipe composée d'étudiant⋅es et d'enseignant⋅es est en charge de l'animation du collectif par la préparation des réunions, le suivi des projets et le lien avec le CÉVU auprès de qui elle fait régulièrement des points d'avancement. À la fin de chaque semestre ou année, l'équipe d'animation peut être renouvellée avec d'autres membres du collectif.

Toutes les deux à quatre semaines, le CIS se réunit pour une séance plénière de partage, de travail et de discussions. Des questionnaires sont envoyées avant chacune d'entre elle pour déterminer une date satisfaisant un maximum de membres du collectif.

# Les travaux du collectif
## En cours
*Rejoignez l'équipe Mattermost pour plus d'information sur les projets en cours !*

## Passés

### Label Ingénierie Soutenable
Lancement officialisé au semestre A21 !

### Réunions de travail P20
Au semestre de printemps 2020 ont eu lieu 8 réunions plénières à distance : Retours d’expérience (Api ID dans la santé et UV évaluation environnementale), stratégies des entreprises face aux enjeux environnementaux, réflexions sur les low-techs (GB, GU, GP, IM), Matériaux et éco-ingénierie, terminologie (passage de CI *durable* à CI *soutenable*) etc..

À l'issue de ces travaux, le collectif a mis en exergue 5 enjeux pour la formation UTC, comme présenté dans l'image ci-dessous.
![positionnement_utc](../assets/images/posts/positionnement_utc.png)


### Séminaire du 2 juillet
Le 2 juilet 2020 s'est tenu un séminaire du CIS avec l'objectif d'élaborer des *scenarii* de formation permettant de répondre aux cinq enjeux présentés sur l'image du prévédente. Cette réflexion s'est basé sur le point de départ suivant :
> Au regard des enjeux de soutenabilité contemporains, l’UTC a vocation à former des ingénieurs :
    ayant une vision systémique ;
    contribuant à une économie circulaire réelle.

L'image ci-dessous présente les conclusions de ce séminaire qui s'est déroulé en ligne et auquel ont participé 16 étudiant⋅es et enseignant⋅es.
![conclusions séminaire](../assets/images/posts/conclusion_seminaire.png)

### Lien avec les départements
Au sein du CIS sont présents des médiateurs et médiatrices faisant le lien avec chaque département. Des présentations du CIS ont ainsi été faites auprès des départements GB, GI et TSH au semestre P20, ainsi que devant le Conseil d'Administration.

### Séminaire inter-UT 2020
Des membres du collectif ont participé à l'animation d'un atelier sur l'ingénierie soutenable au cours du seminaire inter-UT de l'été 2020 durant deux demi-journées en visioconférence.
Des perspectives sur la thématique de la formation et sensibilisation des enseignant⋅es aux enjeux climat et soutenabilité ont émergées.

### PR sur la formation des enseignant⋅es
Un travail de fond sur le sujet de la formation des enseignant⋅es a été réalisé cet été dans le cadre d'une UV PR par une étudiante du collectif, encadrée par des enseignants du collectif. Les fruits de ce travail seront présentés lors de la prochaine réunion plénière du collectif.

### Veille internationale
Un travail de veille internationale sur les formations à l'ingénierie soutenable a été réalisé par un étudiant du collectif cet été. Les fruits de ce travail seront présentés lors de la prochaine réunion plénière du collectif.

### Dispositif de sensibilisation : Rentrée climat UTC 2020
Le CIS a été commanditaire d'un projet GE37 sur la création d'un dispositif de sensibilisation des étudiants de l'UTC, en particulier autour de la rentrée universitaire de septembre.
Un groupe très motivé d'étudiant⋅es et enseignant⋅es ont pris le relai au cours de l'été pour former quarante animateur⋅rices à la Fresque du Climat et préparer les événements en ligne de la rentrée climat.

Pendant deux semaines, 35 Fresques du Climat et 4 Big Confs ont eu lieu pendant l'intégration ! Il s'est agit d'ateliers et de conférences au sujet des enjeux environnementaux qui ont réuni plus de 800 participant⋅es !

![RCUTC](../assets/images/posts/RUTC.jpg)

##### Pour finir, quelques ressources
Ressources, bibliographie et webographie pour un campus et une formation durables dans les UT... Ne pas hésiter à compléter ;)
- [Pad ressources ID en UT](https://pad.picasoft.net/p/Ressources_pour_un_campus_durable)
- [Pad ressources CIS UTC prospection](https://pad.picasoft.net/p/CID_UTC_-_Prospection)

------------


[^note]: _Picasoft est une association de l'Université de Technologie de Compiègne fondée en 2016 par des étudiants et des enseignants. Elle a pour objet de promouvoir et défendre une approche libriste, inclusive, respectueuse de la vie privée, respectueuse de la liberté d'expression, respectueuse de la solidarité entre les humains et respectueuse de l'environnement, notamment dans le domaine de l'informatique._
