---
title: "CR réunion informelle du CÉVU sur le plan de relance d'activité"
layout: post
date: 2020-04-23 22:00                           # Laissez 2099 si c'est un brouillon. Pour l'heure,
                                                 # mettez au moins 2h avant l'heure actuelle (bug GitLab)
image: /assets/images/profile_pictures/all.png   # Apparaît lorsque vous partagez le lien
headerImage: true                               # Mettez 'true' pour mettre l'image précédente en entête du post
category: blog
author: eluscevu
---
# Introduction

Gwénaëlle, Maël, Yasmine, Pierre et Solène (élu.e.s CÉVU) ont participé à une réunion ce jeudi après-midi avec notre directeur Philippe Courtier et des membres du CÉVU (Pierre Feissel, Pierre Morizet, Nicolas Damay, Nathalie Molines) ainsi que le directeur à la formation et à la pédagogie, le responsable de la formation ingénieur, la directrice générale des services et le directeur-adjoint.

Le seul point à l'ordre du jour était l'examen du projet de plan de reprise des activités.

# Projet de plan de reprise des activités

L'UTC est en avance de phase sur le PRA par rapport aux autres universités des Hauts de France. 

Le PRA sera examiné au CHSCT lundi prochain, à la suite de cette réunion le directeur devrait faire un mail à tout le monde pour informer la communauté UTCéenne du contenu de ce plan. Puis après mise à jour en fonction des consignes gouvernementales fin avril et après les avis formels des CHST, Codir, CEVU et CT les 4,5 et 6 mai. Le document devrait paraître officiellement le 7 mai pour une entrée en vigueur le 12 mai.

Présence des doctorants et des stagiaires (M2 et projets de fin d'étude) pas encore arbitrée au niveau national. La reprise d'activité sera progressive.

Le télétravail demeurera la règle. Le lien social entre les collègues devra être reconstruit. 
Chacun des collègues en télétravail devront être présents au moins une fois par semaine au sein de l'UTC pour pouvoir discuter.

Si rebond épidémique au sein de l'UTC : re-fermeture de l'UTC. => Cette reprise des activités doit être progressive.

Le plan de reprise comporte plusieurs phases, nous vous présentons ici les grandes lignes des étapes qui seraient prévues, qui ne sont pas des informations officielles et arrêtées.

### *Phase 1*

Date théorique : 12 mai
Remise des bâtiments de l'UTC en conditions opérationnelles.
Accès limité pour une partie des services.

Objectif : diviser par un facteur 2 les effectifs présents globalement et dans les bureaux. Appliqués pendant les phases 2 à 4.

### *Phase 2*

Date théorique : 18 mai
Faire revenir les personnels de l'UTC, du CNRS et des partenaires dans l'établissement. Dure une semaine. Pas les stagiaires a priori, mais le reste des personnels de laboratoire, dont les doctorants.
 

### *Phase 3*

Date théorique : 25 mai
Dure plusieurs semaines, a priori jusqu'au 31 août. 

Les étudiants, à l'exception des doctorants et des stagiaires ne sont pas autorisés à rentrer sur le site. 
A voir : est ce que les doctorants (et la question se pose encore plus pour les stagiaires) pourront venir à l'UTC ?
On attend l'arbitrage national.

*Question* : est-ce que les jobs étu qui ont lieu pendant l'été pour préparer la rentrée pourront avoir lieu ? (sur les deux dernières semaines de juillet notamment)
--> Question en cours d'étude.

### *Phase 4*

Date théorique : 1e septembre

L'UTC demeure fermée au public
Pic ouvert que pour la restauration du midi. Globalement, la question de la restauration étudiante demeure à finaliser avec le CROUS.
Activités associatives suspendues mais autorisées sous dérogation si des mesures de distanciation sociale sont prises. Hall des sports fermée.

*Question fin des stages P20* : les stages doivent être terminés pour le début des cours (jusqu'au 10 septembre), tolérance pour la fin de la première semaine de cours, mais pas plus loin (jusqu'au 17 septembre maximum, avec les cours à rattraper)

*Question Intégration : possibilité de revoir les consignes si la situation sanitaire est favorable* ? : Reprise très progressive pour éviter un rebond --> principe de précaution prime. L'integ il ne faut pas y compter, le TUC non plus sauf si l'organisation garantit les mesures de distanciation sociale (on peut être assez septique sur cette possibilité).

### *Phase 5*

Date théorique : 14 septembre
Selon l'évolution de la crise, l'UTC reprendrait un fonctionnement nominal à la fin de la phase 4.
Des mesures de distanciation sociales devront être prises pour les rassemblements ou les enseignements et ces derniers pourront éventuellement se dérouler en partie à distance, tout comme le télétravail pourra être privilégié selon l'évolution de la crise.


## 1. Validation des UV

#### 1.a. Ce que nous souhaitions particulièrement mettre en avant côté élus étudiants : 

- Résultat des UV : A, B ou réserve
- Réserve levée grâce à un rattrapage à distance
Les deuxièmes jurys pourraient devenir les rattrapages puisque pas de réorientation
Si pas acquisition des compétences : "annulé" et non F. Important : "annulé" car symboliquement un F est fort pour un étudiant et que cela laisse une trace dans les dossiers (d'ici quelques semestres, on oubliera facilement que ces F étaient liés au Covid)
- Des modalités d'évaluation allégées et variées : des projets, des problèmes à résoudre etc, port folio des TD, assiduité prise en compte...

#### 1.b. Ce qui a été décidé

- **La deadline du 12 mai est confirmée pour que chaque responsable d'UV précise les modalités d'évaluation de chaque UV**
- **Il n'y aura pas d'examens en présentiel.**
- L'important pour les responsable de la formation est de garder des objectifs pédagogique et vérifier que les étudiant.e.s aient acquis les compétences pour valider les UVs.
- Privilégier le contrôle continu.
- Mettre en place une notation à trois niveaux : A, B et refusé. 
- Un message à été transmis aux enseignants pour demander de se concentrer sur les notions essentielles du programme et revoir les exigences.
- **Décision de communiquer aux étudiants qu'il n'y aura pas de réorientation ce semestre.**
*Et si la décision a été prise le semestre dernier ?* --> Complément d'instruction, pas tranché, surement au cas par cas, pas de réponses pour l'instant

#### 1.c. Démarche à suivre pour les étudiants en difficulté ou décrochage dans une UV au moins

- Tout étudiant en difficulté doit se signaler auprès de son conseiller ou resp pédago
- Ils réflechissent ensuite ensemble à différentes solutions pour pallier la situation (cas par cas évidemment)

Les possibilités sont ensuite de :

- décider d'une interruption d'études en cas extrême
- décider de se désinscrire à certaines UVs pour se concentrer sur d'autres, on ne passera pas d'examen sur ces UVs, il n'y aura donc pas de F dans le dossier et l'étudiant pourra se réinscrire plus tard à l'UV

Pour les UVs auxquelles l'étudiant souhaitera se présenter aux examens :

- En cas de gros soucis au dernier moment il reste l'option d'avoir pour résultat "ABSENCE" à l'examen
- Si l'étudiant fait l'examen le résultat pourra être A, B, ou F

Généraliser la "réserve" à la place du F n'est pas faisable car les enseignants n'auront pas le temps de prendre contact avec tant d'étudiants et pour l'inscription dans les UVs à la rentrée ce n'est pas gérable si plein d'étudiant doivent attendre la levée des réserves. Et pas souhaitable selon le directeur car il ne souhaite pas déresponsabiliser les étudiants (si problème il y a les étudiants doivent se manifester en amont).

Ainsi on reste sur des modalités existantes et donc faisable et connues, ne nécessitant pas de toucher au règlement etc. L'idée est de privilégier le contact direct, la proximité entre l'étudiant et son conseiller ou resp pédagogique pour gérer la situation et trouver une solution adaptée à chaque cas. 

Les élus étudiants ont été invités à produire un sondage pour avoir des retours des étudiants sur les évaluations intermédiaires.


## 2. Point sur les départs à l'étranger :
Pour les échanges dans l'espace Shenghen : ok pour A20 théoriquement, en dehors de l'espace Shenghen : départ peu probables pour A20 vu la situation. On ne peut pas vraiment en dire plus. 

**2 cas de figure**
1. Situation actuelle : les français ne sont pas auorisés à quitter le territoire ; on ne sait pas quand ce sera levé
2. On ne connait pas la position des différentes universités partenaires qui ont des difficultés similaires

## 3. Organisation des enseignements à la rentrée
La DFP y réfléchit mais ce n'est pas encore abouti, les cours auront bien lieu mais nécessitent des mesures de distanciation sociale.

## 4. Attestation de l'UTC pour déménager

"Selon la CDEFI, un déménagement de votre logement est autorisé dès lors que vous cochiez la case "raisons familiales impérieuses" sur l'autorisation de déplacement dérogatoire et que vous produisiez une attestation de la direction de l'école comme quoi les cours sont terminés en présentiel jusqu'à la rentrée et en précisant le jour et le lieu de votre déménagement."
Nous avons demandé à la direction de produire une telle attestation et cela va être fait prochainement (ce sera téléchargeable sur l'ENT).

## 5. Point sur la communication aux étudiants et au personnel
Demande de faire un mail récapitulatif en complément du live facebook.

il y aura bien des emails officiels de la DFP pour préciser les informations (en début de semaine prochaine).

## 6. TOEIC
Les collègues des langues sont sur le pont.

## 7. Questions complémentaires

- *Est-ce qu'on peut demander de refaire un stage TN09 si les conditions de fin de stage sont dégradées ?*

Pari très risqué il vaut mieux finir. Offres de stages en diminution, conditions en automne pas sûres.

______________________________

Nous vous invitons à re visionner le live facebook qui a suivi pour tout complément d'informations ! [https://www.facebook.com/utcompiegne/videos/839427696576184/](https://www.facebook.com/utcompiegne/videos/839427696576184/)

*Comme d'habitude, pour toute question n'hésitez pas à nous envoyer un mail ou à poster un message sur notre équipe Mattermost !* ;)
